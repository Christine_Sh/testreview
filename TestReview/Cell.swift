//
//  Cell.swift
//  TestReview
//
//  Created by Khrystyna Shevchuk on 5/13/16.
//  Copyright © 2016 Khrystyna Shevchuk. All rights reserved.
//

import UIKit

class Cell: UITableViewCell {
    
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var titleNoteLabel: UILabel!
    @IBOutlet weak var descriptionNoteLabel: UILabel!
}